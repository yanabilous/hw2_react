import './ProductCard.scss';
import React, { Component } from "react";
import Button from "../button/Button";
import { faHeart, faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";

class ProductCard extends Component {

    state = {
        isProductLiked: this.props.liked || false
    }

    render() {

        const { id, name, price, imageUrl, color } = this.props.product
        const { activateModal, toggleLiked } = this.props

        return (

            <div className="card">

                <div className="cardImage"
                     style={{
                         backgroundImage: `url(${imageUrl})`
                     }}>
                    <FontAwesomeIcon
                        icon={ faHeart }
                        onClick={() => {
                            toggleLiked(this.props.product, this.state.isProductLiked);
                            this.setState((state) => { return {isProductLiked: !state.isProductLiked }})
                        }}
                        style={{
                            color: `${this.state.isProductLiked ? "red" : "#1165B2"}`
                        }}

                    />
                </div>

                <h3 key={name}>{name}</h3>
                 <p className="cardPrice" key={id}>Price: $ {price}</p>

                <div className="cardColor">
                    <p>Color: </p>
                    <div className="cardColor" style={{ backgroundColor: `${color}`, borderRadius: '50%' }}/>
                </div>

                {/*<p className="cardPrice" key={id}>$ {price}</p>*/}
                <div>

                <Button
                    text="Add"
                    onClick={() => {
                        activateModal(this.props.product)
                    }}
                />
                    </div>

            </div>
        )
    }
}


export default ProductCard


