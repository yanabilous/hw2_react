import React, { Component } from 'react';

import './Button.scss';


class Button extends Component {

    render() {
 const { btnBackgroundColor} = this.props;
        return(
            <button
                type='button'
                onClick={this.props.onClick}
                style={{background: btnBackgroundColor}}
            >{this.props.text}</button>
        )
    }
}

export default Button;


