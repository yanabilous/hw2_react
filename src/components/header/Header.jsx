import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faHeart, faCartShopping, faStar, faBasketShopping} from "@fortawesome/free-solid-svg-icons";
import './Header.scss';

class Header extends Component {



    render() {

        return(
            <>
                <div className="header">
                    <div className="headerTitle">
                        <h1>Nike Shop</h1>
 <img className="logo" src={"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Logo_NIKE.svg/1200px-Logo_NIKE.svg.png"} style={{maxWidth: "500px", height: "50px"}}/>
                    </div>

                    <div style={{
                        flexGrow: 1
                    }}/>
                    <div className="headerCart">

                        <FontAwesomeIcon icon={faBasketShopping} />
                        {this.props.cart.length > 0 && <div className="headerQuantity">{this.props.cart.length}</div>}
                    </div>
                    <div className="headerWishList">

                        <FontAwesomeIcon icon={faHeart} />
                        {this.props.liked.length > 0 && <div className="headerQuantity">{this.props.liked.length}</div>}
                    </div>
                </div>

            </>
        )
    }
}

export default Header;


